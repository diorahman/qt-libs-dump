TARGET = use-dynamic
TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle

INCLUDEPATH += ../src
LIBS += -L../build-dynamic -llib1-dyn

SOURCES = main.cpp
