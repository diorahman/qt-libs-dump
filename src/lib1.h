#ifndef LIB1_H
#define LIB1_H

#if defined LIB1
#define LIB1_COMMON_DLLSPEC Q_DECL_EXPORT
#else
#define LIB1_COMMON_DLLSPEC Q_DECL_IMPORT
#endif

#include <QObject>

class LIB1_COMMON_DLLSPEC Lib1 : public QObject
{
  Q_OBJECT

public:
    Lib1(QObject * parent = 0);
    ~Lib1(){}
    void hello();
};

#endif

