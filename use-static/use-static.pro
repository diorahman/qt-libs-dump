TARGET = use-static
TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle

INCLUDEPATH += ../src
LIBS += -L../build-static -llib1

SOURCES = main.cpp
